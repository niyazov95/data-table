import {ITableHeaders} from '../../models/data-table/table.header.interface';

export const MOVIE_HEADERS: ITableHeaders[] = [
  {
    key: 'name',
  },
  {
    key: 'genre',
  },
  {
    key: 'season',
    colSize: 'xs',
    sorting: true,
  },
  {
    key: 'network',
    colSize: 'm',
  },
  {
    key: 'premierDate',
    colSize: 'm',
    sorting: true,
  },
];
//
// name: string;
// premierDate: number;
// network: string[];
// season: number;
// genre: EGenre[];
