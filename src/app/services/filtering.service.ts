import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FilteringService {
  onFilteringData: Subject<any> = new Subject<any>();

  constructor() {}

  filterData(data) {
    this.onFilteringData.next(data);
  }
}
