import {EGenre} from './genre.enum';

export interface IMovie {
  name: string;
  premierDate: string;
  network: string[];
  season: number;
  genre: EGenre[];
}
