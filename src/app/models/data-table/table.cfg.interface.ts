export class ITableCfg {
  totalItems: number;
  pageSize: number;
  totalPages: number;
  pageNum: number;

  constructor(data: any[], pageSize: number = 10, pageNum = 1) {
    this.pageNum = pageNum;
    this.totalItems = data.length;
    this.pageSize = pageSize;
    this.totalPages = Math.ceil(data.length / pageSize);
  }
}
