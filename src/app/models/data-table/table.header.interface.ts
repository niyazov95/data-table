export interface ITableHeaders {
  key: string;
  colSize?: 'm' | 's' | 'xs';
  sorting?: boolean;
}
