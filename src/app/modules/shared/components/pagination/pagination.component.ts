import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.sass'],
})
export class PaginationComponent {
  @Input() pageNum: number;
  @Input() totalPages: number;
  @Input() scrollIntoViewId: string;
  @Output() setActivePage: EventEmitter<number> = new EventEmitter<number>();
  constructor() {}

  setAsActivePage(page: number) {
    this.pageNum = page;
    this.setActivePage.emit(this.pageNum);

    const firstElement = document.getElementById(this.scrollIntoViewId);
    if (firstElement) {
      firstElement.scrollIntoView({block: 'start', behavior: 'smooth'});
    } else {
      const scrollToTop = window.setInterval(() => {
        const pos = window.pageYOffset;
        if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
        } else {
          window.clearInterval(scrollToTop);
        }
      }, 7);
    }
  }

  totalPagesArray() {
    return Array.from({length: this.totalPages}, (v, k) => k + 1).slice(this.pageNum - 1, this.pageNum + 5);
  }

  isActivePage(page: number) {
    return this.pageNum === page;
  }
}
