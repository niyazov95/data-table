import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ITableCfg} from '../../../../models/data-table/table.cfg.interface';
import {ITableHeaders} from '../../../../models/data-table/table.header.interface';
import {EGenre} from '../../../../models/movie/genre.enum';
@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent implements OnChanges {
  @Input() tableHeaders: ITableHeaders[];
  @Input() tableData: any[];
  @Input() tableTitle: string;
  @Input() tableShadowless: boolean;
  @Input() tableBordered: boolean;
  triggerPagination: boolean;
  dataToShow: any[];

  tableCfg: ITableCfg;

  constructor() {}

  ngOnChanges() {
    this.getData();
  }

  pageChange(e: any) {
    const from = (e - 1) * this.tableCfg.pageSize;
    const to = +from + +this.tableCfg.pageSize;
    console.log(from + ' --- ' + to);
    this.dataToShow = this.tableData.slice(from, to);
  }

  getData() {
    this.triggerPagination = true
    this.tableCfg = new ITableCfg(this.tableData, this.tableCfg?.pageSize, 1);
    this.dataToShow = this.tableData.slice(0, this.tableCfg.pageSize);
    setTimeout(() => this.triggerPagination = false, 1);
  }

  getEnumValue(genre: EGenre) {
    return EGenre[genre];
  }

  sort(header: ITableHeaders) {
    if (header.key !== 'premierDate') {
      this.dataToShow.sort((a, b) => {
        return a[header.key] - b[header.key];
      });
    } else {
      this.dataToShow.sort((a, b) => {
        return new Date(a[header.key]).getTime() - new Date(b[header.key]).getTime();
      });
    }
  }
}
