import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaginationComponent} from './components/pagination/pagination.component';
import {DataTableComponent} from './components/data-table/data-table.component';
import {FormsModule} from '@angular/forms';
import {FilterPipe} from '../../pipes/filter.pipe';

@NgModule({
  declarations: [PaginationComponent, DataTableComponent, FilterPipe],
  exports: [DataTableComponent],
  imports: [CommonModule, FormsModule],
})
export class SharedModule {}
