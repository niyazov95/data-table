import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-movie-wrapper',
  template: `
    <div class="row">
      <div class="col-12 mt-5">
        <h2>DATA TABLE</h2>
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: [],
})
export class MovieWrapperComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
