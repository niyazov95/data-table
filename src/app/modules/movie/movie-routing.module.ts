import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MovieWrapperComponent} from './movie-wrapper.component';
import {MovieComponent} from './components/movie/movie.component';

const routes: Routes = [
  {
    path: '',
    component: MovieWrapperComponent,
    children: [
      {
        path: '',
        component: MovieComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovieRoutingModule {}
