import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieRoutingModule } from './movie-routing.module';
import { MovieWrapperComponent } from './movie-wrapper.component';
import {SharedModule} from '../shared/shared.module';
import { MovieComponent } from './components/movie/movie.component';
import { MovieFilterFormComponent } from './components/movie/movie-filter-form/movie-filter-form.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [MovieWrapperComponent, MovieComponent, MovieFilterFormComponent],
    imports: [
        CommonModule,
        MovieRoutingModule,
        SharedModule,
        ReactiveFormsModule
    ]
})
export class MovieModule { }
