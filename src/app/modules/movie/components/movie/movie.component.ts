import {Component, OnInit} from '@angular/core';
import {ITableHeaders} from '../../../../models/data-table/table.header.interface';
import {IMovie} from '../../../../models/movie/movie.interface';
import {MOVIE_MOCK} from '../../../../const/movie/movie.mock';
import {MOVIE_HEADERS} from '../../../../const/data-table/table-header.const';
import {FilteringService} from '../../../../services/filtering.service';

@Component({
  selector: 'app-movie',
  template: `
    <div class="row">
      <div class="col-12 mt-2">
        <app-movie-filter-form></app-movie-filter-form>
      </div>
      <div class="col-12 mt-2">
        <app-data-table [tableHeaders]="movieTableHeader" [tableData]="filteredData"></app-data-table>
      </div>
    </div>
  `,
  styles: [],
})
export class MovieComponent implements OnInit {
  movieTableHeader: ITableHeaders[];
  movieTableData: IMovie[];
  filteredData: IMovie[];
  constructor(private filteringService: FilteringService) {}

  ngOnInit(): void {
    this.movieTableData = MOVIE_MOCK;
    this.movieTableHeader = MOVIE_HEADERS;
    this.filteredData = this.movieTableData;
    this.filteringService.onFilteringData.subscribe((filters: any) => {
      const keys = Object.keys(filters);
      let data = this.movieTableData;
      keys.forEach(key => {
        if (key === 'name' && filters[key]) {
          data = this.movieTableData.filter(item => item[key].toLowerCase().indexOf(filters[key].toLowerCase()) !== -1);
        } else if (key === 'premierDate' && filters[key]) {
          data = data.filter(item => item[key] === filters[key]);
        } else if (key === 'genre' && filters[key]) {
          data = data.filter(item => item[key].includes(+filters[key]));
        }
      });
      this.filteredData = data;
    });
  }
}
