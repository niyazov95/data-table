import {Component, OnInit} from '@angular/core';
import {EGenre} from '../../../../../models/movie/genre.enum';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FilteringService} from '../../../../../services/filtering.service';

@Component({
  selector: 'app-movie-filter-form',
  templateUrl: './movie-filter-form.component.html',
  styleUrls: ['./movie-filter-form.component.scss'],
})
export class MovieFilterFormComponent implements OnInit {
  public movieGenres = EGenre;
  filteringForm: FormGroup;

  constructor(private fb: FormBuilder, private filteringService: FilteringService) {
    this.filteringForm = this.fb.group({
      name: [''],
      genre: [null],
      premierDate: [null],
    });
  }

  ngOnInit(): void {}

  filter() {
    this.filteringService.filterData(this.filteringForm.value);
  }
}
